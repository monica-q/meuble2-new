<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package WordPress
 * @subpackage MEUBLE 
 * @since MEUBLE 1.0
 */
get_header();
?>

    <!-- cs-1st -->
    <section class="m-cs_hero-wrp">
        <div class="m-cs_inner">
            <div>
                <h2 class="m-cs_title">PROJECT
                    <span>プロジェクト</span>
                </h2>
            </div>
        </div>
        <!-- breadcrumbs -->
        <div class="m-breadcrumbs">
            <ul>
                <li><a href="<?=esc_url( home_url("/") );?>">Home</a></li>
                <li>PROJECT</li>
            </ul>
        </div>
        <!-- //breadcrumbs -->
    </section>
    <!-- //cs 1st -->
    
    <!-- cs-2nd -->
    <section class="mn-box970">
        <div class="m-cs_bnr-wrp is-prjct" data-aos="fade-up" data-aos-duration="2000">
            <div class="m-cs_inner is-sm">
                <h3 class="m-cs_title">モーブルの導入事例や実績・様々な取り組みをご紹介いたします。</h3>
                <ul class="mn-cat-p">
                    <?php
                        $post_category = @$_GET['cat'];
                                
                        if( isset($post_category) && !empty($post_category) ) {
                            $post_category = @$_GET['cat'];
                            
                        }else{
                            $post_category = "";
                        }
                            $include_taxonomy = array();
                            
                            $args = array(
                                    'taxonomy' => 'case_cat',
                                    'orderby' => 'term_id',
                                    'order'   => 'ASC',
                                    'hide_empty' => false
                                );
                            $categories_taxonomy = get_categories($args); 
                            $cats = get_categories($args);
                            $terms = get_terms('hide_empty=0');
                            foreach($cats as $cat) {
                            if( $cat->slug ==  $post_category ) {
                                $set_active = "is-active";
                            }
                    ?>
                    <li>
                        <a href="?cat=<?php echo $cat->slug; ?>" own-attr="<?php echo $cat->term_id; ?>">
                            <span><?php echo $cat->name; ?></span>
                        </a>
                    </li>
                    <?php
                        }
                    ?>
                 </ul> 
            </div>
        </div>
    </section>
    <!-- //cs-2nd -->
    <!-- cs 3rd -->
    <section class="m-cs_cards-wrp is-prjct">
        <div class="m-cs_inner">
            <div class="gap gap-0 gap-0-xs">
                <?php 
                    $count = 0;
                    $cs_pg = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
                    $max_post_page = 9;
                    if( isset($post_category) && !empty($post_category) ) {
                        $query_cs = new WP_Query(
                            array(
                                'post_type'     =>'case_study', 
                                'post_status'   =>'publish', 
                                'posts_per_page' => -1, 
                                'orderby'        => 'publish_date',
                                'order'         => 'DESC',
                                'paged'         => $cs_pg,
                                'tax_query' => [
                                    [
                                        'taxonomy' => 'case_cat',
                                        'field' => 'slug',
                                        'terms' => $post_category,
                                        'order' => 'DESC',
                                        'include_children' => true,
                                        
                                    ]
                                ]
                        )); 
                        }else{
                            $query_cs = new WP_Query(
                                array(
                                    'post_type'     =>'case_study', 
                                    'post_status'   =>'publish', 
                                    'posts_per_page' => -1, 
                                    'orderby'        => 'publish_date',
                                    'order'         => 'DESC',
                                    'paged'         => $cs_pg
                            )); 
                        }
                    
                        $total_pages = $query_cs->max_num_pages;
                        $stat_class = "";
                        $cat_n = "";
                    
                        if ( $query_cs->have_posts() ) :
                                
                        while ( $query_cs->have_posts() ) : $query_cs->the_post(); $count++; 
                        $categories = get_the_terms( $post->ID, 'case_cat' );
                        $cat_name = $categories[0]->name;
                ?>
                <div class="md-6 xs-6" data-aos="fade-up" data-aos-duration="1000">
                    <a href="<?=the_permalink();?>" class="m-cs_card">
                        <?php if(has_post_thumbnail()){ ?>
                        <div class="m-cs_card-head"  style="background-image: url('<?php echo the_post_thumbnail_url("large") ?>')"></div>
                        <?php }else{ ?>
                        <div class="m-cs_card-head"  style="background-image: url('https://dummyimage.com/600x400/3b3a3b/ffffff.png&text=No+Image')"></div>
                        <?php } ?>
                        <div class="m-cs_card-body">
                            <h3 class="m-cs_title"><?=the_title();?></h3>
                            <p>Client：<?=the_field('client');?></p>
                        </div>
                    </a>
                </div>
                <?php endwhile;
                    ?>
                <?php else: ?>
                <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
                <?php endif; ?>
            </div>     
        </div>
    </section>
    <!-- //cs 3rd -->

    <!-- cs 4th -->
    <?=get_template_part("template-parts/recruit-temp");?>
    <!-- //cs 4th -->

    <!-- contact -->
    <?=get_template_part("template-parts/contact-temp");?>
    <!-- //contact -->

<?php
get_footer();
?>
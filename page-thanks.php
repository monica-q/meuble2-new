<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package MEUBLE
 */

get_header();
?>
<style>
#back_contact {
    display: none;
}
</style>

<div class="contact-page">
    <h2 class="m-contact-title">Contact <span>株式会社モーブル へのお問い合わせ</span></h2>

    <div class="m-breadcrumbs">
        <ul>
            <li><a href="<?=esc_url( home_url("/") );?>">Home</a></li>
            <li>Contact</li>
        </ul>
    </div>

    <div class="contact-box box2">
        <h2>送信されました</h2>
        <h3>お問い合わせありがとうございました。</h3>
        <p>ご入力いただいたメールアドレス宛に受付確認メールをお送りしましたのでご確認ください。
        確認メールが届いていない場合にはメールアドレスが誤っているか、迷惑メールフォルダに振り分けられている可能性がありますので、お手数ですがご確認をお願いいたします。</p>
        <p>なお、お問い合わせ内容につきましては、通常○営業日程度にてご連絡をいたします。
        万一、届かない場合は、送信トラブルの可能性もありますので、大変お手数ではございますが、もう一度フォームよりお問い合わせいただくかお電話にてお問い合わせをお願いいたします。</p>
        <a href="<?=esc_url( home_url("/") );?>">HOMEへ</a>
    </div>

 </div>

<?php
get_footer();
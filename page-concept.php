<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package WordPress
 * @subpackage MEUBLE 
 * @since MEUBLE 1.0
 */
get_header();
?>

    <!-- cs-1st -->
    <section class="m-cs_hero-wrp is-concept">
        <div class="m-cs_inner">
            <div>
                <h2 class="m-cs_title">CONCEPT
                    <span>モーブルのコンセプト</span>
                </h2>
            </div>
        </div>
        <!-- breadcrumbs -->
        <div class="m-breadcrumbs">
            <ul>
                <li><a href="<?=esc_url( home_url("/") );?>">Home</a></li>
                <li>CONCEPT</li>
            </ul>
        </div>
        <!-- //breadcrumbs -->
    </section>
    <!-- //cs 1st -->
    
    <!-- cs-2nd -->
    <section class="m-cs_cp-wrp">
        <div class="m-cs_inner">
            <h2 class="m-cs_title" data-aos="fade-up" data-aos-duration="2000">導入のキャッチコピーが<br>入ります。</h2>
            <div class="gap gap-10 gap-10-xs">
                <div class="md-6 xs-6 mb-20-xs" data-aos="fade-up" data-aos-duration="2000">
                    <a href="#" class="m-cs_cp-item">
                        <span class="num">01.</span>
                        <p>メインのコンセプト文章の<br>タイトルが入ります。</p>
                    </a>
                </div>
                <div class="md-6 xs-6 mb-20-xs" data-aos="fade-up" data-aos-duration="2000">
                    <a href="#" class="m-cs_cp-item">
                        <span class="num">02.</span>
                        <p>創業からこれまでについて書かれた文章の<br>タイトルが入ります。</p>
                    </a>
                </div>
                <div class="md-6 xs-6 mb-0" data-aos="fade-up" data-aos-duration="2000">
                    <a href="#" class="m-cs_cp-item">
                        <span class="num">03.</span>
                        <p>リビングからライフスタイルに変革している事について書かれたタイトルが入ります。</p>
                    </a>
                </div>
                <div class="md-6 xs-6" data-aos="fade-up" data-aos-duration="2000">
                    <a href="#" class="m-cs_cp-item">
                        <span class="num">04.</span>
                        <p>モノづくりではなくコトづくりのタイトル。</p>
                    </a>
                </div>
            </div>
        </div>
    </section>
    <!-- //cs-2nd -->

    <!-- cs 3rd -->
    <section class="m-cs_cp-bg" style="background:url(<?=get_template_directory_uri()?>/assets/img/concept/img-01.jpg) center center/cover no-repeat;">
        <div class="m-cs_inner">
            <div class="m-cs_half" data-aos="fade-up" data-aos-duration="2000">
                <div class="m-cs_half-cntr">
                    <h4 class="m-cs_title-line"><span>Concept</span></h4>
                    <h3 class="m-cs_title">創業からこれまでについての<br>テキストテキストテキストテキ</h3>
                    <p>創業からこれまでについて書かれた文章。テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスストテキストテキスト</p>
                </div>
            </div>
        </div>
    </section>
    <section class="m-cs_cp-bg" style="background:url(<?=get_template_directory_uri()?>/assets/img/concept/img-02.jpg) center center/cover no-repeat;">
        <div class="m-cs_inner is-right">
            <div class="m-cs_half" data-aos="fade-up" data-aos-duration="2000">
                <div class="m-cs_half-cntr">
                    <h4 class="m-cs_title-line"><span>History</span></h4>
                    <h3 class="m-cs_title">創業からこれまでについての<br>テキストテキストテキストテキ</h3>
                    <p>創業からこれまでについて書かれた文章。テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスストテキストテキスト</p>
                </div>
            </div>
        </div>
    </section>
    <section class="m-cs_cp-bg" style="background:url(<?=get_template_directory_uri()?>/assets/img/concept/img-03.jpg) center 30%/cover no-repeat;">
        <div class="m-cs_inner">
            <div class="m-cs_half" data-aos="fade-up" data-aos-duration="2000">
                <div class="m-cs_half-cntr">
                    <h4 class="m-cs_title-line"><span>life style</span></h4>
                    <h3 class="m-cs_title">創業からこれまでについての<br>テキストテキストテキストテキ</h3>
                    <p>創業からこれまでについて書かれた文章。テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスストテキストテキスト</p>
                </div>
            </div>
        </div>
    </section>
    <!-- //cs 3rd -->

    <!-- cs 4th -->
    <section class="m-cs_cp-other">
        <div class="m-cs_inner">
            <div class="m-cs_img">
                <img src="<?=get_template_directory_uri()?>/assets/img/concept/img-04.png" alt="" class="is-wide">
            </div>
            <div class="m-cs_cntr" data-aos="fade-up" data-aos-duration="2000">
                <div class="m-cs_title tc"> モノづくりではなくコトづくり<br class="v-pc">テキストテキストテキストテキスト</div>
                <p>トテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストトテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストスト</p>
            </div>
        </div>
    </section>
    <!-- //cs 4th -->

    <!-- cs 5th -->
    <?=get_template_part("template-parts/recruit-temp");?>
    <!-- //cs 5th -->

    <!-- contact -->
    <?=get_template_part("template-parts/contact-temp");?>
    <!-- //contact -->

<?php
get_footer();
?>
<?php
/**
 * The main template file
 *Template Name: Homepage
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package WordPress
 * @subpackage MEUBLE 
 * @since MEUBLE 1.0
 */
get_header();
?>



<!-- banner -->
<section class="m-top-banner">
    <div id="m-top-slider" class="m-top-banner-slider owl-carousel owl-theme">
        <div class="m-top-banner-item m-top-banner-item-1 item">
            <div class="m-top-bnr-img">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/top/bnr-img02.png" alt="" class="is-wide">
            </div>
            <div class="m-top-bnr-cont">
                <h2>
                    時代に合わせた<br>
                    ライフスタイルの提案で<br>
                    テキストテキスト。
                </h2>
            </div>
        </div>
        <div class="m-top-banner-item m-top-banner-item-2 item">
            <div class="m-top-bnr-img">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/top/bnr-img02.png" alt="" class="is-wide">
            </div>
            <div class="m-top-bnr-cont">
                <h2>
                    時代に合わせた<br>
                    ライフスタイルの提案で<br>
                    テキストテキスト。
                </h2>
            </div>
        </div>
        <div class="m-top-banner-item m-top-banner-item-3 item">
            <div class="m-top-bnr-img">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/top/bnr-img02.png" alt="" class="is-wide">
            </div>
            <div class="m-top-bnr-cont">
                <h2>
                    時代に合わせた<br>
                    ライフスタイルの提案で<br>
                    テキストテキスト。
                </h2>
            </div>
        </div>
    </div>
    <div class="scroll-to-bot">
        <a href="#footer">Scroll</a>
    </div>
</section>
<!-- end of banner -->

<!-- news -->
<section class="m-top-news">
    <div class="m-news-cntr" data-aos="fade-up" data-aos-duration="2000">
        <div class="m-tit">
            <h2>News</h2>
            <p>最新情報</p>
        </div>
        <?php 
        // news query
        $news_query = new WP_Query(array('post_type'=>'news', 'post_status'=>'publish', 'posts_per_page'=>3)); ?>
        
        <?php if ( $news_query->have_posts() ) : ?>
        
        <ul class="m-news-list">
        
            <!-- the loop -->
            <?php while ( $news_query->have_posts() ) : $news_query->the_post(); ?>
                <li id="post-<?php the_ID(); ?>" <?php post_class('m-news-list-item'); ?>>
                    <a href="<?php the_permalink(); ?>">
                        <span class="m-news-date"><?php the_time('Y m.d'); ?></span><span class="m-news-text"><?php the_title(); ?></span><span class="m-news-tag">New</span>
                    </a>
                </li>
            <?php endwhile; ?>
            <!-- end of the loop -->
        </ul>
        <div class="m-news-all">
            <a href="<?php bloginfo('url'); ?>/news">View All</a>
        </div>
        <?php wp_reset_postdata(); ?>
        <?php else : ?>
            <h4 class="rob-no-post"><?php _e( 'Sorry, no news matched your criteria.' ); ?></h4>
        <?php endif; ?>
    </div>
</section>
<!-- end of news -->

<!-- professionals -->
<section class="m-top-prof">
    <div class="gap gap-0-xs">
        <div class="md-6 xs-12">
            <div class="m-top-prof-cont-left">
                <div class="m-top-cont-wrp">
                    <h2 class="m-prof-tit" data-aos="fade-up" data-aos-duration="2000">
                        <span>We’re professional</span><br>
                        <span>life style company.</span>
                    </h2>
                    <div class="m-prof-cont">
                        <p class="m-desc" data-aos="fade-up" data-aos-duration="2000">
                            コンセプトの文章テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスストテキストテキスト
                        </p>
                        <div class="m-btn t-center" data-aos="fade-up" data-aos-duration="2000">
                            <a href="<?php bloginfo('url'); ?>/concept">Concept</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="md-6 xs-12">
            <div class="m-top-prof-cont-right"></div>
        </div>
    </div>
</section>
<!-- end of professionals -->

<!-- brand -->
<section class="m-top-brand">
    <div class="m-top-brand-cntr">
        <div class="m-tit">
            <h2>Brand</h2>
            <p>モーブルの取り扱いブランド</p>
        </div>
        <div class="m-tagline">
            <p>
                私たちは独自の目線で厳選した家具作りと日本唯一のIKASAS<br>正規取扱店を行っています。（ダミー）
            </p>
        </div>
        <div class="gap gap-15 gap-0-xs" data-aos="fade-up" data-aos-duration="2000">
            <div class="md-4 xs-12">
                <a href="#" class="m-card-brand">
                    <div class="m-card-brand-img">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/top/brand-card01.png" alt="" class="is-wide">
                    </div>
                    <div class="m-card-brand-ico">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/top/brand01.svg" alt="" class="brand01">
                    </div>
                </a>
            </div>
            <div class="md-4 xs-12">
                <a href="#" class="m-card-brand">
                    <div class="m-card-brand-img">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/top/brand-card02.png" alt="" class="is-wide">
                    </div>
                    <div class="m-card-brand-ico">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/top/brand02.svg" alt="" class="brand02">
                    </div>
                </a>
            </div>
            <div class="md-4 xs-12">
                <a href="#" class="m-card-brand">
                    <div class="m-card-brand-img">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/top/brand-card03.png" alt="" class="is-wide">
                    </div>
                    <div class="m-card-brand-ico">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/top/brand03.svg" alt="" class="brand03">
                    </div>
                </a>
            </div>
        </div>
    </div>
</section>
<!-- end of brand -->

<!-- case study -->
<section class="m-top-case-study">
    <div class="m-tit">
        <h2>PROJECT</h2>
        <p>プロジェクト</p>
    </div>
    <section class="m-cs_cards-wrp">
        <div class="m-cs_inner">
            <div class="gap gap-0 gap-0-xs">
            <?php 
                $query_cs = new WP_Query(
                            array(
                                'post_type'     =>'case_study', 
                                'post_status'   =>'publish', 
                                'posts_per_page' => 4, 
                                'orderby'        => 'publish_date',
                                'order'         => 'DESC'
                            ));  

                ?>

                <?php if ( $query_cs->have_posts() ) : ?>
                    <?php while ( $query_cs->have_posts() ) : $query_cs->the_post();  ?>
                        <div class="md-6 xs-6" data-aos="fade-up" data-aos-duration="1000">
                            <a href="<?=the_permalink();?>" class="m-cs_card">
                                <?php if(has_post_thumbnail()){ ?>
                                <div class="m-cs_card-head"  style="background-image: url('<?php echo the_post_thumbnail_url("large") ?>')"></div>
                                <?php }else{ ?>
                                <div class="m-cs_card-head"  style="background-image: url('https://dummyimage.com/600x400/3b3a3b/ffffff.png&text=No+Image')"></div>
                                <?php } ?>
                                
                                <div class="m-cs_card-body">
                                    <h3 class="m-cs_title"><?=the_title();?></h3>
                                    <p>Client：<?=the_field('client');?></p>
                                </div>
                            </a>
                        </div>
                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
        </div>
    </section>
    <div class="m-line-btn_cntr">
        <a href="#" class="m-btn_line">Read More</a>
    </div>
</section>
<!-- end of case study -->

<!-- corporate information -->
<div class="cntr">
    <div class="know">
        <h4 class="mn-line-btm" data-aos="fade-up" data-aos-duration="2000">
            <span>企業情報</span>
        </h4>   
        <ul class="know-lst">
            <li>
                <a href="#">
                    <div class="kl-crd" data-aos="fade-up" data-aos-duration="2000">
                        <div class="thmbnl-img">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/thumbnail/thumb01.png" alt="" class="is-wide">
                        </div>
                        <div class="cntnt" data-aos="fade-up" data-aos-duration="2000">
                            <h3>代表挨拶</h3>
                        <div>
                    </div>
                </a>
            </li>
            <li>
                <a href="#">
                    <div class="kl-crd" data-aos="fade-up" data-aos-duration="2000">
                       <div class="thmbnl-img">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/thumbnail/thumb02.png" alt="" class="is-wide">
                        </div>
                        <div class="cntnt" data-aos="fade-up" data-aos-duration="2000">
                            <h3>経営理念</h3>
                        <div>
                    </div>
                </a>
            </li>
            <li>
                <a href="#">
                    <div class="kl-crd" data-aos="fade-up" data-aos-duration="2000">
                        <div class="thmbnl-img">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/thumbnail/thumb04.png" alt="" class="is-wide">
                        </div>
                        <div class="cntnt" data-aos="fade-up" data-aos-duration="2000">
                            <h3>会社概要</h3>
                        <div>
                    </div>
                </a>
            </li>
            <li>
                <a href="#">
                    <div class="kl-crd" data-aos="fade-up" data-aos-duration="2000">
                        <div class="thmbnl-img">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/thumbnail/thumb03.png" alt="" class="is-wide">
                        </div>
                        <div class="cntnt" data-aos="fade-up" data-aos-duration="2000">
                            <h3>企業活動</h3>
                        <div>
                    </div>
                </a>
            </li>
        </ul>
    </div>
</div>
<!-- end of corporate information -->

<!-- cs 4th -->
<?php get_template_part("template-parts/recruit-temp");?>
<!-- //cs 4th -->

<!-- contact -->
<?php get_template_part("template-parts/contact-temp");?>
<!-- //contact -->


<?php
get_footer();
?>
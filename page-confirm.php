<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package MEUBLE
 */

get_header();
?>
<style>
.con-form-btn {
    display: -webkit-box;
    display: -webkit-flex;
    display: -moz-box;
    display: -ms-flexbox;
    display: flex;
}
#back_contact {
    display: block;
}
</style>
<div class="contact-page">
    <h2 class="m-contact-title">Contact <span>株式会社モーブル へのお問い合わせ</span></h2>

    <div class="m-breadcrumbs">
        <ul>
            <li><a href="<?=esc_url( home_url("/") );?>">Home</a></li>
            <li>Contact</li>
        </ul>
    </div>

    <div class="contact-box" data-aos="fade-up" data-aos-duration="2000">
        <h2>内容をご確認ください</h2>
        <p>こちらの内容でお間違いなければ<br>
        「送信する」ボタンを押してください。</p>
    </div>

        <!-- <div class="contact-fields">
            <label for="">お名前 <span>必須</span></label>
            <span class="contact-ans">田中太郎</span>
        </div>
        <div class="contact-fields">
            <label for="">会社名</label>
            <span class="contact-ans">株式会社モーブル </span>
        </div>
        <div class="contact-fields">
            <label for="">メールアドレス <span>必須</span></label>
            <span class="contact-ans">info@example.com </span>
        </div>
        <div class="contact-fields">
            <label for="">確認用メールアドレス <span>必須</span></label>
            <span class="contact-ans">info@example.com </span>
        </div>
        <div class="contact-fields">
            <label for="">郵便番号 <span>必須</span></label>
            <span class="contact-ans number">8310006 </span>
        </div>
        <div class="contact-fields">
            <label for="">住所 <span>必須</span></label>
            <span class="contact-ans">福岡県大川市中古賀956-1 </span>
        </div>
        <div class="contact-fields">
            <label for="">電話番号 <span>必須</span></label>
            <span class="contact-ans number">0944881955 </span>
        </div>
        <div class="contact-fields">
            <label for="">お問い合わせ内容 <span>必須</span></label>
            <span class="contact-ans">ご質問・お問い合わせなどお気軽にどうぞ。 </span>
        </div> -->

        <?php if ( have_posts() ) : while ( have_posts() ) : the_post();
				the_content();
				
        endwhile; else: ?>
        <p>Sorry, no posts matched your criteria.</p>
        <?php endif; ?>
</div>

<?php
get_footer();
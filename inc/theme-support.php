<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package TEMPLATE NAME
 * 
 */


/*
 * ----------------------------------------------------------------------------------------
 *  THEME SUPPORT
 * ----------------------------------------------------------------------------------------
 */
add_theme_support( 'post-thumbnails' );
add_theme_support( 'post-formats', array( 'aside', 'gallery', 'link', 'image', 'qoute', 'status', 'video', 'audio', 'chat' ) );


/*
 * ----------------------------------------------------------------------------------------
 *  REGISTER THE NAV MENU
 * ----------------------------------------------------------------------------------------
 */
function nav_theme_setup() {
    add_theme_support('menus');

    register_nav_menu('primary', 'Header Navigation');
    register_nav_menu('secondary', 'Footer Navigation');
}
add_action('init', 'nav_theme_setup');


//Numeric Pagination
function wp_pagination() {
    global $wp_query;
    $big = 12345678;
    $page_format = paginate_links( array(
        'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
        'format' => '?paged=%#%',
        'current' => max( 1, get_query_var('paged') ),
        'total' => $wp_query->max_num_pages,
        'type'  => 'array',
        'prev_text' => __('<'),
        'next_text' => __('>'),
    ) );
    if( is_array($page_format) ) {
                $paged = ( get_query_var('paged') == 0 ) ? 1 : get_query_var('paged');
                echo '<div class="pagination"><ul>';
                foreach ( $page_format as $page ) {
                        echo "<li>$page</li>";
                }
                echo '</ul></div>';
    }
}
function my_php_Include($params = array()) {
    extract(shortcode_atts(array('file' => 'default'), $params));
    extract(shortcode_atts(array('rss_url' => 'default'), $params));
    $_GET["rss_url"] = $rss_url;
    ob_start();
    include(get_theme_root().'/'.get_template()."/$file.php");
    return ob_get_clean();
}
add_shortcode('myphp', 'my_php_Include');

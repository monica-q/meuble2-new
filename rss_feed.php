<?php
include_once( ABSPATH . WPINC . '/feed.php' );
$rss_url = $_GET['rss_url'];
$rss = fetch_feed( $rss_url );
//var_dump($rss);
if ( !is_wp_error( $rss ) ) {
    $maxitems = $rss->get_item_quantity( 3 );
    $rss_items = $rss->get_items( 0, $maxitems );
}
?>
<?php //if ( !empty( $maxitems ) ) : ?>
<?php if ($maxitems == 0) echo '<li>RSSデータがありませんでした。</li>';
else
foreach ( $rss_items as $item ) : ?>
<li class="news-list-detail-item" data-aos="fade-up" data-aos-duration="2000">
    <a href="<?php echo $item->get_permalink(); ?>">
        <ul class="mn-nw-lst">
            <li>
                <?php
                $first_img = '';
                if ( preg_match( '/<img.+?src=[\'"]([^\'"]+?)[\'"].*?>/msi',$item->get_content(), $matches )) {
                    $first_img = $matches[1];
                } 
                ?>
                <?php if ( !empty( $first_img ) ) : ?>
                <img src="<?php echo esc_attr( $first_img ); ?>" alt="" class="is-wide" />
                <?php else: ?>
                <img src="https://dummyimage.com/600x400/3b3a3b/ffffff.png&text=No+Image" alt="<?php the_title(); ?>" class="is-wide">
                <?php endif; ?>
            </li>
            <li>
                <div class="news-text-cont">
                    <span class="news-text-det"><?php echo $item->get_title(); ?></span>
                </div>
                <div class="news-date">
                    <span class="news-det-date">
                        <?php echo $item->get_date('Y.m.d'); ?>
                    </span>
                </div>
            </li>
        </ul>
    </a>
</li>
<?php endforeach; ?>
<?php //endif; ?>
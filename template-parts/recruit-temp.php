<section class="m-cs_bnr-wrp has-bg" data-aos="fade-up" data-aos-duration="2000">
    <div class="rec_div-parent">
        <div class="m-cs_bnr-inner">
            <h3 class="m-cs_bnr-title">RECRUIT</h3>
            <h2 class="m-cs_bnr-phar">私たちはお客様に喜びや感動をお届けする「創造業」です。</h2>
            <div class="m-cs-rec-btn">
                <a href="#" class="m-cs_btn-ei">採用情報を見る</a>
            </div>
        </div>
    </div>
</section>
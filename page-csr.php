<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package WordPress
 * @subpackage MEUBLE 
 * @since MEUBLE 1.0
 */
get_header();
?>

    <!-- cs-1st -->
    <section class="m-cs_hero-wrp is-csr">
        <div class="m-cs_inner is-csr">
            <div>
                <h2 class="m-cs_title">CSR,SDGs
                    <span>企業活動</span>
                </h2>
            </div>
        </div>
        <!-- breadcrumbs -->
        <div class="m-breadcrumbs">
            <ul>
                <li><a href="<?=esc_url( home_url("/") );?>">Home</a></li>
                <li><a href="<?=esc_url( home_url("company-profile") );?>">COMPANY</a></li>
                <li>CSR,SDGs</li>
            </ul>
        </div>
        <!-- //breadcrumbs -->
    </section>
    <!-- //cs 1st -->
    
    <!-- cs-2nd -->
    <section class="cntr csr-wrp mn-ovrhdn">
        <div class="mn-box10">
            <h4 class="mn-line-btm" data-aos="fade-up" data-aos-duration="2000">
                <span>SDGsについて</span>
            </h4>    
            <div class="sec1">
                <h3 data-aos="fade-up" data-aos-duration="2000">持続可能な世界のためにモーブル ができること。</h3>
                <p data-aos="fade-up" data-aos-duration="2000">
                SDGs（エスディージーズ：Sustainable Development Goals　持続可能な開発目標）とは、持続可能な世界を実現するための17のゴール・169のターゲットから構成されており、地球上の「誰一人として取り残さない」ことを誓っている国際目標です。
                </p>
            </div>
        </div>
        <div class="mn-box8">
            <div class="sec2">
                <ul class="sec2-lst">
                    <li data-aos="fade-up" data-aos-duration="2000">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/csr/txt_01.png" alt="" class="is-wide">
                    </li>
                    <li data-aos="fade-up" data-aos-duration="2000">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/csr/img_01.png" alt="" class="is-wide">
                    </li>
                </ul>
                <div class="sec2-a">
                    <p data-aos="fade-up" data-aos-duration="2000">株式会社モーブル は持続可能な開発目標（SDGs）を支援しています</p>
                </div>
            </div>
            <div class="sec3">
                <h3 data-aos="fade-up" data-aos-duration="2000">
                    モーブルはSDGsの17の目標のうち<br class="mn-pc">以下の４つに力を入れて取り組んでいます
                </h3>
                <div class="sec3-a">
                    <div class="gap gap-10 gap-10-xs">
                        <div class="md-3 xs-6 mb-15-xs" data-aos="fade-up" data-aos-duration="2000">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/csr/img_02.png" alt="" class="is-wide">
                        </div>
                        <div class="md-3 xs-6" data-aos="fade-up" data-aos-duration="2000">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/csr/img_03.png" alt="" class="is-wide">
                        </div>
                        <div class="md-3 xs-6" data-aos="fade-up" data-aos-duration="2000">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/csr/img_04.png" alt="" class="is-wide">
                        </div>
                        <div class="md-3 xs-6" data-aos="fade-up" data-aos-duration="2000">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/csr/img_05.png" alt="" class="is-wide">
                        </div>
                    </div>
                </div>
                <div class="csr-lst">
                    <ul class="sec3-lst">
                        <li data-aos="fade-up" data-aos-duration="2000">一、マイクロプラスチックを出しません</li>
                        <li data-aos="fade-up" data-aos-duration="2000">一、バイオマスプラスチックの研究開発に取り組んでいます</li>
                        <li data-aos="fade-up" data-aos-duration="2000">一、CO2削減に取り組んでいます</li>
                        <li data-aos="fade-up" data-aos-duration="2000">一、有害なガスを出しません</li>
                        <li data-aos="fade-up" data-aos-duration="2000">一、100%リサイクル可能な素材を使用しています</li>
                    </ul>
                </div>
            </div>
            <div class="sec4">
                <h4 class="mn-line-btm" data-aos="fade-up" data-aos-duration="2000">
                    <span>CSR活動について</span>
                </h4>    
                <div class="mn-box965">      
                    <div class="csr-box">
                        <h3 data-aos="fade-up" data-aos-duration="2000">地元・大川市の保育園にLiterieマットを寄贈いたしました。</h3>
                        <p data-aos="fade-up" data-aos-duration="2000">大川中央保育園様</p>
                        <?php if( have_rows('image_gallery') ): ?>
                            <?php while( have_rows('image_gallery') ): the_row(); 
                                $image = get_sub_field('image1');
                                $image2 = get_sub_field('image2');
                                $image3 = get_sub_field('image3');
                                $image4 = get_sub_field('image4');
                                $image5 = get_sub_field('image5');
                                $image6 = get_sub_field('image6');
                                $image7 = get_sub_field('image7');
                                $image8 = get_sub_field('image8');
                                $image9 = get_sub_field('image9');
                                $image10 = get_sub_field('image10');
                            ?> 
                        <div id="mn-gallery" data-aos="fade-up" data-aos-duration="2000">
                            <div class="gap gap-8 gap-0-xs">
                                <div class="md-8 xs-12">
                                    <div id="panel">
                                        <?php if( !empty( $image ) ): ?>
                                            <img id="mn-dsply-tmbnl" class="is-wide" src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" class="is-wide" />
                                        <?php else: ?>
                                            <img id="mn-dsply-tmbnl" class="is-wide" src="<?php echo get_template_directory_uri(); ?>/assets/img/csr/thumb_gray.png" alt="<?php the_title(); ?>">
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <div class="md-4 xs-12">
                                    <ul id="mn-thumbnail">
                                        <li>
                                            <?php if( !empty( $image ) ): ?>
                                                <img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" class="is-wide" />
                                            <?php else: ?>
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/csr/thumb_gray.png" alt="<?php the_title(); ?>" class="is-wide">
                                            <?php endif; ?>
                                        </li>
                                        <li>
                                            <?php if( !empty( $image2 ) ): ?>
                                                <img src="<?php echo esc_url($image2['url']); ?>" alt="<?php echo esc_attr($image2['alt']); ?>" class="is-wide" />
                                            <?php else: ?>
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/csr/thumb_gray.png" alt="<?php the_title(); ?>" class="is-wide">
                                            <?php endif; ?>
                                        </li>
                                        <li>
                                            <?php if( !empty( $image3 ) ): ?>
                                                <img src="<?php echo esc_url($image3['url']); ?>" alt="<?php echo esc_attr($image3['alt']); ?>" />
                                            <?php else: ?>
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/csr/thumb_gray.png" alt="<?php the_title(); ?>" class="is-wide">
                                            <?php endif; ?>
                                        </li>
                                        <li>
                                            <?php if( !empty( $image4 ) ): ?>
                                                <img src="<?php echo esc_url($image4['url']); ?>" alt="<?php echo esc_attr($image4['alt']); ?>" class="is-wide" />
                                            <?php else: ?>
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/csr/thumb_gray.png" alt="<?php the_title(); ?>" class="is-wide">
                                            <?php endif; ?>
                                        </li>
                                        <li>
                                            <?php if( !empty( $image5 ) ): ?>
                                                <img src="<?php echo esc_url($image5['url']); ?>" alt="<?php echo esc_attr($image5['alt']); ?>" class="is-wide" />
                                            <?php else: ?>
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/csr/thumb_gray.png" alt="<?php the_title(); ?>" class="is-wide">
                                            <?php endif; ?>
                                        </li>
                                        <li>
                                            <?php if( !empty( $image6 ) ): ?>
                                                <img src="<?php echo esc_url($image6['url']); ?>" alt="<?php echo esc_attr($image6['alt']); ?>" class="is-wide" />
                                            <?php else: ?>
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/csr/thumb_gray.png" alt="<?php the_title(); ?>" class="is-wide">
                                            <?php endif; ?>
                                        </li>
                                        <li>
                                            <?php if( !empty( $image7 ) ): ?>
                                                <img src="<?php echo esc_url($image7['url']); ?>" alt="<?php echo esc_attr($image7['alt']); ?>" class="is-wide" />
                                            <?php else: ?>
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/csr/thumb_gray.png" alt="<?php the_title(); ?>" class="is-wide">
                                            <?php endif; ?>
                                        </li>
                                        <li>
                                            <?php if( !empty( $image8 ) ): ?>
                                                <img src="<?php echo esc_url($image8['url']); ?>" alt="<?php echo esc_attr($image8['alt']); ?>" class="is-wide" />
                                            <?php else: ?>
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/csr/thumb_gray.png" alt="<?php the_title(); ?>" class="is-wide">
                                            <?php endif; ?>
                                        </li>
                                        <li>
                                            <?php if( !empty( $image9 ) ): ?>
                                                <img src="<?php echo esc_url($image9['url']); ?>" alt="<?php echo esc_attr($image9['alt']); ?>" class="is-wide" />
                                            <?php else: ?>
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/csr/thumb_gray.png" alt="<?php the_title(); ?>" class="is-wide">
                                            <?php endif; ?>
                                        </li>
                                        <li>
                                            <?php if( !empty( $image10 ) ): ?>
                                                <img src="<?php echo esc_url($image10['url']); ?>" alt="<?php echo esc_attr($image10['alt']); ?>" class="is-wide" />
                                            <?php else: ?>
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/csr/thumb_gray.png" alt="<?php the_title(); ?>" class="is-wide">
                                            <?php endif; ?>
                                        </li>
                                    </ul>
                                </div>
                            </div>   
                        </div>
                        <?php endwhile; ?>
                        <?php endif; ?>
                        <div class="cntnts">
                            <p data-aos="fade-up" data-aos-duration="2000">【内容】</p>
                            <p data-aos="fade-up" data-aos-duration="2000">
                                園児さんとサポートして下さる保育スタッフの皆さんへ、お昼寝マットやクッションなど<br>合計５０点のリテリー商品を寄贈
                            </p>
                        </div>
                        <div class="cntnts is-a">
                            <p data-aos="fade-up" data-aos-duration="2000">【背景】</p>
                            <p data-aos="fade-up" data-aos-duration="2000">
                                地方の人口減と少子化問題が進む昨今、本社所在地である福岡県大川市も例外ではない。<br>ここ数十年の間、毎年高齢者は増えていき、子どもは減り続けているのが現状。<br>大川市でモノづくりを生業とする当社でも、「何か少しでも地域に貢献できることがないか」という想いで実現<br>「洗える」「ムレない」「へたりにくい」という特徴が成長期の子どもに最適で、また、近年多い新生児のうつぶせ寝による窒息死を防ぐ「沈み込みにくい高反発性」「90%以上の空気層からなる通気性」を有している商品です。<br>「90%以上の空気層からなる通気性」を有している
                            </p>
                        </div>
                        <div class="csr-lnk" data-aos="fade-up" data-aos-duration="2000">
                            <a href="">>ニュース記事を見る</a>
                        </div>
                    </div>   
                    <div class="csr-box">
                        <h3 data-aos="fade-up" data-aos-duration="2000">屋久島地杉バス停プロジェクトにてベンチに置くクッション材を提供</h3>
                        <p data-aos="fade-up" data-aos-duration="2000">屋久島地杉プロジェクト様</p>
                        <?php if( have_rows('yakushima_chisugi_project') ): ?>
                            <?php while( have_rows('yakushima_chisugi_project') ): the_row(); 
                                $image = get_sub_field('image1');
                                $image2 = get_sub_field('image2');
                                $image3 = get_sub_field('image3');
                                $image4 = get_sub_field('image4');
                                $image5 = get_sub_field('image5');
                                $image6 = get_sub_field('image6');
                                $image7 = get_sub_field('image7');
                                $image8 = get_sub_field('image8');
                                $image9 = get_sub_field('image9');
                                $image10 = get_sub_field('image10');
                            ?> 
                        <div id="mn-gallery" data-aos="fade-up" data-aos-duration="2000">
                            <div class="gap gap-8 gap-0-xs">
                                <div class="md-8 xs-12">
                                    <div id="panel">
                                        <?php if( !empty( $image ) ): ?>
                                            <img id="mn-dsply-tmbnl-a" class="is-wide" src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" class="is-wide" />
                                        <?php else: ?>
                                            <img id="mn-dsply-tmbnl-a" class="is-wide" src="<?php echo get_template_directory_uri(); ?>/assets/img/csr/thumb_gray.png" alt="<?php the_title(); ?>">
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <div class="md-4 xs-12">
                                    <ul id="mn-thumbnail-a">
                                        <li>
                                            <?php if( !empty( $image ) ): ?>
                                                <img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" class="is-wide" />
                                            <?php else: ?>
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/csr/thumb_gray.png" alt="<?php the_title(); ?>" class="is-wide">
                                            <?php endif; ?>
                                        </li>
                                        <li>
                                            <?php if( !empty( $image2 ) ): ?>
                                                <img src="<?php echo esc_url($image2['url']); ?>" alt="<?php echo esc_attr($image2['alt']); ?>" class="is-wide" />
                                            <?php else: ?>
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/csr/thumb_gray.png" alt="<?php the_title(); ?>" class="is-wide">
                                            <?php endif; ?>
                                        </li>
                                        <li>
                                            <?php if( !empty( $image3 ) ): ?>
                                                <img src="<?php echo esc_url($image3['url']); ?>" alt="<?php echo esc_attr($image3['alt']); ?>" />
                                            <?php else: ?>
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/csr/thumb_gray.png" alt="<?php the_title(); ?>" class="is-wide">
                                            <?php endif; ?>
                                        </li>
                                        <li>
                                            <?php if( !empty( $image4 ) ): ?>
                                                <img src="<?php echo esc_url($image4['url']); ?>" alt="<?php echo esc_attr($image4['alt']); ?>" class="is-wide" />
                                            <?php else: ?>
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/csr/thumb_gray.png" alt="<?php the_title(); ?>" class="is-wide">
                                            <?php endif; ?>
                                        </li>
                                        <li>
                                            <?php if( !empty( $image5 ) ): ?>
                                                <img src="<?php echo esc_url($image5['url']); ?>" alt="<?php echo esc_attr($image5['alt']); ?>" class="is-wide" />
                                            <?php else: ?>
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/csr/thumb_gray.png" alt="<?php the_title(); ?>" class="is-wide">
                                            <?php endif; ?>
                                        </li>
                                        <li>
                                            <?php if( !empty( $image6 ) ): ?>
                                                <img src="<?php echo esc_url($image6['url']); ?>" alt="<?php echo esc_attr($image6['alt']); ?>" class="is-wide" />
                                            <?php else: ?>
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/csr/thumb_gray.png" alt="<?php the_title(); ?>" class="is-wide">
                                            <?php endif; ?>
                                        </li>
                                        <li>
                                            <?php if( !empty( $image7 ) ): ?>
                                                <img src="<?php echo esc_url($image7['url']); ?>" alt="<?php echo esc_attr($image7['alt']); ?>" class="is-wide" />
                                            <?php else: ?>
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/csr/thumb_gray.png" alt="<?php the_title(); ?>" class="is-wide">
                                            <?php endif; ?>
                                        </li>
                                        <li>
                                            <?php if( !empty( $image8 ) ): ?>
                                                <img src="<?php echo esc_url($image8['url']); ?>" alt="<?php echo esc_attr($image8['alt']); ?>" class="is-wide" />
                                            <?php else: ?>
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/csr/thumb_gray.png" alt="<?php the_title(); ?>" class="is-wide">
                                            <?php endif; ?>
                                        </li>
                                        <li>
                                            <?php if( !empty( $image9 ) ): ?>
                                                <img src="<?php echo esc_url($image9['url']); ?>" alt="<?php echo esc_attr($image9['alt']); ?>" class="is-wide" />
                                            <?php else: ?>
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/csr/thumb_gray.png" alt="<?php the_title(); ?>" class="is-wide">
                                            <?php endif; ?>
                                        </li>
                                        <li>
                                            <?php if( !empty( $image10 ) ): ?>
                                                <img src="<?php echo esc_url($image10['url']); ?>" alt="<?php echo esc_attr($image10['alt']); ?>" class="is-wide" />
                                            <?php else: ?>
                                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/csr/thumb_gray.png" alt="<?php the_title(); ?>" class="is-wide">
                                            <?php endif; ?>
                                        </li>
                                    </ul>
                                </div>
                            </div>   
                        </div>
                        <?php endwhile; ?>
                        <?php endif; ?>
                        <div class="cntnts">
                        <p data-aos="fade-up" data-aos-duration="2000">【内容】</p>
                        <p data-aos="fade-up" data-aos-duration="2000">
                            屋久島地杉プロジェクトに対し、ベンチの上にクッション材を提供をするという形で協力。<br>バス停を使う利用者（島の子どもや観光客）がより快適に過ごせるようになった。
                        </p>
                        <div class="cntnts is-a">
                            <p data-aos="fade-up" data-aos-duration="2000">【背景】</p>
                            <p data-aos="fade-up" data-aos-duration="2000">
                                屋久島地杉プロジェクトの発端は、バス停で待つ人が安心して雨風を防げる場所が欲しかったのがひとつ。<br>
                                樹齢1000年を超える杉を「屋久杉」と呼ぶのに対し、戦後人の手によって植林した杉を「地杉」と呼び、地杉が間伐のタイミングに来ている。「保護するべき自然(屋久杉)」と「産業として活用するべき自然(地杉)」を考え、地杉の活用を見出し、家具や建材にすることによって木としての価値を上げることを目指すもの。
                            </p>
                        </div>
                        <div class="cntnts is-b">
                            <p data-aos="fade-up" data-aos-duration="2000">【前提】</p>
                            <p data-aos="fade-up" data-aos-duration="2000">
                                武蔵野美術大学の非常勤講師である建築家 アンドレア彦根先生指揮のもと、<br>
                                建築を学ぶ学生とともに、これまでの林業とこれからの林業について学びながら、<br>
                                エネルギー・森・木を結び付けられるシンボルとなるバス停の設計・デザインに取り組ん<br>だものである。
                            </p>
                        </div>
                    </div>      
                </div>
            </div>
        </div>
        </div>
        <div class="mn-wrp-m is-a">
            <div class="know">
                <h4 class="mn-line-btm" data-aos="fade-up" data-aos-duration="2000">
                    <span>もっとモーブル を知る</span>
                </h4>   
                <ul class="know-lst">
                    <li class="is-abt">
                        <a href="#">
                            <div class="kl-crd" data-aos="fade-up" data-aos-duration="2000">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/thumbnail/thumb01.png" alt="" class="is-wide">
                                <div class="cntnt" data-aos="fade-up" data-aos-duration="2000">
                                    <h3>代表挨拶<small>MESSAGE</small></h3>
                                <div>
                            </div>
                        </a>
                    </li>
                    <li class="is-abt">
                        <a href="#">
                            <div class="kl-crd" data-aos="fade-up" data-aos-duration="2000">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/thumbnail/thumb02.png" alt="" class="is-wide">
                                <div class="cntnt" data-aos="fade-up" data-aos-duration="2000">
                                    <h3>経営理念<small>PHILOSOPHY</small></h3>
                                <div>
                            </div>
                        </a>
                    </li>
                    <li class="is-abt">
                        <a href="<?=esc_url( home_url("about-us") );?>">
                            <div class="kl-crd" data-aos="fade-up" data-aos-duration="2000">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/thumbnail/thumb04.png" alt="" class="is-wide">
                                <div class="cntnt" data-aos="fade-up" data-aos-duration="2000">
                                    <h3>会社概要<small>ABOUT US</small></h3>
                                <div>
                            </div>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </section>
    <!-- //cs-2nd -->

    <!-- cs 5th -->
    <?=get_template_part("template-parts/recruit-temp");?>
    <!-- //cs 5th -->

    <!-- contact -->
    <?=get_template_part("template-parts/contact-temp");?>
    <!-- //contact -->




    



<script>
(function($){
    $('#mn-thumbnail').delegate('img','click', function(){
        $('#mn-dsply-tmbnl').attr('src',$(this).attr('src').replace('thumb','thumb'));
    });
    $('#mn-thumbnail-a').delegate('img','click', function(){
        $('#mn-dsply-tmbnl-a').attr('src',$(this).attr('src').replace('thumb','thumb'));
    });
}) (jQuery);
</script>




<?php
get_footer();
?>


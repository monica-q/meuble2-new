<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package WordPress
 * @subpackage MEUBLE 
 * @since MEUBLE 1.0
 */
get_header();
?>

    <!-- cs-1st -->
    <section class="m-cs_slider-wrp wrp">
        <div id="cs-slider" class="m-cs_slider owl-carousel owl-theme">
            <?php if(has_post_thumbnail()){ ?>
            <div class="m-cs_item owl-lazy" data-src="<?php echo the_post_thumbnail_url("large") ?>">
            <?php }else{ ?>
                <div class="m-cs_item owl-lazy" data-src="https://dummyimage.com/600x400/3b3a3b/ffffff.png&text=No+Image">
            <?php } ?>
            <?php
                $categories = get_the_terms( $post->ID, 'case_cat' );
                $cat_name = $categories[0]->name;
            ?>
                <div class="cs_item-inner">
                    <h2 class="p_pg">CASE STUDY / <span class="cat"><?php echo $cat_name; ?></span></h2>
                    <span><?php echo $cat->name; ?></span>
                    <h2 class="p_pg-title"><?=get_the_title();?></h2>
                    <p>Client：<?=the_field("client");?></p>
                </div>
            </div>
            <?php 
                $query_cs = new WP_Query(
                            array(
                                'taxonomy' => 'case_cat',
                                'post_type'     =>'case_study', 
                                'post_status'   =>'publish', 
                                'posts_per_page' => -1, 
                                'orderby'        => 'publish_date',
                                'order'         => 'DESC'
                            ));  

                ?>
            <?php if ( $query_cs->have_posts() ) : ?>
               <?php while ( $query_cs->have_posts() ) : $query_cs->the_post();  ?>
                <?php if(has_post_thumbnail()){ ?>
                <div class="m-cs_item owl-lazy" data-src="<?php echo the_post_thumbnail_url("large") ?>">
                <?php }else{ ?>
                    <div class="m-cs_item owl-lazy" data-src="https://dummyimage.com/600x400/3b3a3b/ffffff.png&text=No+Image">
                <?php } ?>
                    <div class="cs_item-inner">
                        <h2 class="p_pg">CASE STUDY</h2>
                        <h2 class="p_pg-title"><?=the_title();?></h2>
                        <p>Client：<?=the_field('client');?></p>
                    </div>
                </div>
                <?php endwhile; ?>
            <?php endif; ?>
        </div>
        <div class="cs-navs"></div>
        <!-- breadcrumbs -->
        <div class="m-breadcrumbs">
            <ul>
                <li><a href="<?=esc_url( home_url("/") );?>">Home</a></li>
                <li><a href="<?=esc_url( home_url("/case-study") );?>">Project</a></li>
                <li><?=get_the_title();?></li>
            </ul>
        </div>
        <!-- //breadcrumbs -->
    </section>
    <!-- //cs 1st -->
    <!-- cs-2nd -->
    <section class="m-cs_bnr-wrp is-single">
        <div class="m-cs_inner is-xs">
            <div class="m-cs_phar">
            <?php
                while ( have_posts() ) :
                    the_post();
                    the_content();
                endwhile; // End of the loop.
            ?>
            </div>
        </div>
    </section>
    <!-- //cs-2nd -->

    <!-- cs 3rd -->
    <section class="m-cs_cards-wrp">
        <div class="m-cs_inner">
            <div class="m-cs_inner_tit">
                <h2>最新のプロジェクトを見る</h2>
            </div>
            <div class="gap gap-0 gap-0-xs">
            <?php 
                $query_cs = new WP_Query(
                            array(
                                'post_type'     =>'case_study', 
                                'post_status'   =>'publish', 
                                'posts_per_page' => 4, 
                                'orderby'        => 'publish_date',
                                'order'         => 'DESC'
                            ));  

                ?>

                <?php if ( $query_cs->have_posts() ) : ?>
                    <?php while ( $query_cs->have_posts() ) : $query_cs->the_post();  ?>
                        <div class="md-6 xs-6" data-aos="fade-up" data-aos-duration="1000">
                            <a href="<?=the_permalink();?>" class="m-cs_card">
                                <?php if(has_post_thumbnail()){ ?>
                                <div class="m-cs_card-head"  style="background-image: url('<?php echo the_post_thumbnail_url("large") ?>')"></div>
                                <?php }else{ ?>
                                <div class="m-cs_card-head"  style="background-image: url('https://dummyimage.com/600x400/3b3a3b/ffffff.png&text=No+Image')"></div>
                                <?php } ?>
                                
                                <div class="m-cs_card-body">
                                    <h3 class="m-cs_title"><?=the_title();?></h3>
                                    <p>Client：<?=the_field('client');?></p>
                                </div>
                            </a>
                        </div>
                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
            <!-- btn -->
            <div class="m-line-btn_cntr">
                <a href="<?=esc_url(home_url("/case-study"));?>" class="m-btn_line">導入事例一覧へ</a>
            </div>
            <!-- //btn -->
        </div>
    </section>
    <!-- //cs 3rd -->

    <!-- cs 4th -->
    <?=get_template_part("template-parts/recruit-temp");?>
    <!-- //cs 4th -->

    <!-- contact -->
    <?=get_template_part("template-parts/contact-temp");?>
    <!-- //contact -->

<?php
get_footer();
?>

<script>

    (function ($) {
        var cs_owl = $('#cs-slider');
        cs_owl.owlCarousel({
				lazyLoad: true,
				center: true,
				items: 1.1,
				nav: true,
				margin: 0,
				padding: 0,
				autoplay: false,
				touchDrag: true,
				pullDrag: true,
				mouseDrag: true,
				loop: true,
				dots: true,
				animateOut: 'fadeOut',
				smartSpeed: 500,
				autoplayHoverPause: true,
                navText: [
                    '<span class="custom-arr">',
                    '<span class="custom-arr">'
                ],
                navContainer: '.cs-navs'
			});
    })(jQuery);

</script>